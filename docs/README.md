# 非官方修正

<!-- 
```bash {.line-numbers cmd=true output=text hide=true run_on_save=true}
tree -d .
``` -->

```bash {.line-numbers}
.
├── cmp_sad-10e            机械工业出版社_《系统分析与设计（原书第10版）》（文家焱）
├── cmp_tast-3e            机械工业出版社_《软件测试的艺术（原书第3版）》（张晓明）
├── cmp_tcpl               机械工业出版社_《C程序设计语言（第2版）》（徐宝文）
├── nup_srag               南京大学出版社_《软件需求分析师岗位指导教程》（桂超）
├── tup_os-3e              清华大学出版社_《计算机操作系统（第3版）》（郁红英）
├── tup_sqa-and-st-2e      清华大学出版社_《软件质量保证与测试（第2版）》（秦航）
├── tup_sr-3e              清华大学出版社_《软件需求（第3版）》（李忠利）
└── tup_swpm-vlet          清华大学出版社_《软件项目管理（微课版）》（李英龙）
```
