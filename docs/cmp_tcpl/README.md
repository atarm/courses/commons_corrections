# 机械工业出版社_《C程序设计语言（第2版）》（徐宝文）

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Chapter 2 - Types, Operators and Expressions](#chapter-2-types-operators-and-expressions)
2. [Appendix A - Reference Manual](#appendix-a-reference-manual)
    1. [A.8 Declarations](#a8-declarations)
        1. [A.8.2 Type Specifiers](#a82-type-specifiers)

<!-- /code_chunk_output -->

## Chapter 2 - Types, Operators and Expressions

2.4 Declarations

1. P31，2.4节标题下第一段第二行
    - 级别：`warning`
    - 译文：后面所带的 **~~变量表~~** 可以包含一个或多个该类型的变量
    - 原文：and contains a list of one or more variables of that type
    - 建议：后面所带的 **变量列表** 可以包含一个或多个该类型的变量

## Appendix A - Reference Manual

### A.8 Declarations

#### A.8.2 Type Specifiers

1. P187，倒数第二段最后一行
    - 级别：`fatal`
    - 译文：volatile对象没有与实现 **~~无关~~** 的语义
    - 原文：There are no implementation-dependent semantics for volatile objects.
    - 建议：volatile对象没有与实现 **相关** 的语义
<!--TODO:实现依赖是否是主流翻译？-->
