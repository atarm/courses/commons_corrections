# 机械工业出版社_《系统分析与设计（原书第10版）》（文家焱）

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [书籍信息](#书籍信息)
2. [关于本非官方修正的相关说明](#关于本非官方修正的相关说明)
3. [非官方修正](#非官方修正)
    1. [全文术语](#全文术语)
    2. [前言](#前言)
    3. [正文](#正文)
        1. [第01章_系统、角色和开发方法](#第01章_系统-角色和开发方法)
        2. [第02章_理解并建模组织系统](#第02章_理解并建模组织系统)
        3. [第03章_项目管理](#第03章_项目管理)
        4. [第04章_信息收集：交互式方法](#第04章_信息收集交互式方法)
        5. [第06章_敏捷建模、原型化方法和Scrum](#第06章_敏捷建模-原型化方法和scrum)

<!-- /code_chunk_output -->

## 书籍信息

- 原书名：Systems Analysis and Design,Tenth Edition
- 原著：KKenneth E. Kendall, Julie E. Kendall
- 原出版社：Pearson Education Inc.
- 书名：系统分析与设计（原书第10版）
- 译者：文家焱 施平安
- 出版社：机械工业出版社
- ISBN：978-7-111-63525-3

## 关于本非官方修正的相关说明

1. 本修正为非官方修正，仅供参考，本人水平极有限，期待对本非官方修正进行指正、或补充本人的遗漏
1. 本非官方修正主要包含可能会影响理解英文原文原意的翻译（即：比较严重的翻译问题）
1. 下述`原文`、`原翻译`指翻译版的内容，`英文原文`指英文原版的内容，`修正翻译`指本非官方修正翻译的内容

## 非官方修正

### 全文术语

1. `SDLC`
    - 原文：系统开发生命期
    - 修正：系统开发生命<ins>周</ins>期
    - 修正说明：不确定`系统开发生命期`是否是旧时对`SDLC`的翻译，目前主流翻译为`系统开发生命周期`

### 前言

1. Pviii第03段第01行
    - 原文：<del>介绍了系统开发生命期（System Development Life Cycle, SDLC）的三种主要方法体系</del>、敏捷开发、UML面向对象分析以及使用它们的原因和情况
    - 修正：<ins>介绍了三种主要方法体系：系统开发生命周期（System Development Life Cycle, SDLC）</ins>、敏捷开发、UML面向对象分析以及使用它们的原因和情况
    - 修正说明：应该是笔误
1. Px第01段第01行
    - 原文：本章帮助学生确定是使用<del>SLDC</del>、敏捷方法还是面向对象系统分析与设计方法来进行系统开发
    - 修正：本章帮助学生确定是使用<ins>SDLC</ins>、敏捷方法还是面向对象系统分析与设计方法来进行系统开发
    - 修正说明：应该是笔误

### 正文

#### 第01章_系统、角色和开发方法

1. P13图1.5
    - 原文：从<del>用例场景</del>导出活动图
    - 修正：从<ins>用例</ins>导出活动图
    - 英文原文：derive activity diagrams from use cases
    - 修正说明：用例包括用例图和用例场景/用例规约两种描述方式
1. P13图1.5
    - 原文：修改模型图和完成<del>规范</del>
    - 修正：修改模型图和完成<ins>规格说明</ins>
    - 英文原文："modify diagrams and complete specifications"
    - 修正说明：`specification`一般翻译为`规格说明`或`规约`
1. P13倒数第01段
    - 原文：面向对象方法通常关注<del>小规模开发和快速迭代，有时称为螺旋模型</del>
    - 修正：面向对象方法通常关注<ins>小规模且快速迭代开发（小规模开发和快速迭代开发有时称为螺旋模型）</ins>
    - 英文原文：Object-oriented methodologies often focus on small, quick iterations of development, sometimes called the spiral model.
    - 修正说明：面向对象方法不是螺旋模型，螺旋模型的特征包括小规模且快速迭代开发
1. P15第01段第02行
    - 原文：将开源社区分成4种不同类型：<del>专门的</del>（ad hoc）、标准化的（standardized）、有组织的（organized）和商业的（commercial）
    - 修正：将开源社区分成4种不同类型：<ins>特设的</ins>（ad hoc）、标准化的（standardized）、有组织的（organized）和商业的（commercial）
    - 修正说明：将`ad hoc`翻译成`特设的`，产生的歧义更小一点
1. P16第05段第02行
    - 原文：可能需要先访问万维网站<del>www.prenhall.com/kendall</del>
    - 修正：可能需要先访问万维网站<ins>http://www.pearsonhighered.com/kendall</ins>
    - 修正说明：<http://www.prenhall.com/kendall>无法访问，原因不明

#### 第02章_理解并建模组织系统

1. P18标题
    - 原文：<del>了解组织系统及组织系统建模</del>
    - 修正：<ins>理解并建模组织系统</ins>
    - 英文原文：Understanding and Modeling Organizational Systems
    - 修正说明：原文容易将`"组织"`理解为动词，即容易误解为"了解如何组织系统及如何组织团队以进行系统建模"

#### 第03章_项目管理

1. P45"3.1 项目启动"下第01段第01行
    - 原文：一些提议的项目将经历各种各样的评估阶段，这些评估由你（或者你和你的团队完成）；其他项目不需要也不应该经历那么多评估
    - 英文原文：Some of the projects suggested will survive various stages of evaluation to be worked on by you(or you and your team); others will not and should not get that far.
    - 说明：不理解原文和英文原文要表达的意思，"others"指"其他人"还是指"其他项目"？
1. P64"3.5.1 工作分解结构"下第03段
    - 原文：每个任务或活动包含该活动的一个<del>可交付产品或有形成果</del>
    - 修正：每个任务或活动包含该活动的一个<ins>可交付的或明确的成果</ins>
    - 英文原文：Each task or activity contains one deliverable, or tangible outcome, from the activity.
    - 修正说明：`tangible`可以根据语境翻译成不同的中文，其中此处翻译成`明确的`更符合原文意思（不仅是`有形的`，`无形的`服务也是可以的）
1. P64"3.5.1 工作分解结构"下第05段
    - 原文：每个任务有一个负责人负责监督和控制<del>性能</del>
    - 修正：每个任务有一个负责人负责监督和控制<ins>执行情况</ins>
    - 英文原文：Each task has a responsible person monitoring and controlling performance
1. P681倒数第03段第01行
    - 原文：PERT是Program Evaluation and Review Technique（<del>程序</del>评审技术）的缩写词
    - 修正：PERT是Program Evaluation and Review Technique（<del>计划</del>评审技术）的缩写词
    - 修正说明：`PERT`的主流翻译是`计划评审技术`
1. P68最后一行
    - 原文：（2）表示在可以<del>承担</del>新活动之前<del>需要完成哪些活动（所谓的优先权）</del>
    - 修正：（2）表示在可以<ins>开展</ins>新活动之前<ins>哪些活动需要先完成（即：前置）</ins>
    - 英文原文：(2) indicate which activities need to be completed before new activities may be undertaken (which is called _precedence_)
    - 修正说明：`precedence`在此处应翻译成`前置`，而不是翻译成`优先权`
1. P72倒数第02段第01行
    - 原文：鱼骨图的价值是系统地列出所有可能发生的<del>问题</del>
    - 修正：鱼骨图的价值是<ins>可以</ins>系统地列出所有风险
    - 英文原文：When using a fishbone diagram, you systematically list all the problems that can possibly occur
    - 修正说明：problems that can possibly occur即`风险`（未来可能发生的、会带来损失的事件）

#### 第04章_信息收集：交互式方法

1. P97图4.1、P97倒数第02段
    - 原文：<del>准备</del>面谈对象
    - 修正：<ins>预约</ins>面谈对象
    - 英文原文：prepare the interviewee, preparing the interviewee
    - 修正说明：原文翻译不符合中文习惯

#### 第06章_敏捷建模、原型化方法和Scrum

1. P165-166
    - 原文：`operations`翻译成<del>`运营`</del>
    - 修正：`operations`翻译成<ins>`运维`</ins>
    - 修正：`operation`在技术领域一般翻译成`运维`，在业务领域一般翻译成`运营`，在某些领域翻译成`操作`（如：`operating system`翻译成`操作系统`）或`运筹`（如：`operations research`翻译成`运筹学`）
    - 修正说明：应当根据不同语境（领域）翻译成相应术语
