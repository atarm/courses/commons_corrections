module.exports = {
    types: [
        //the forward
        { value: "feat",            name: "feat:            a new feature" },//create: add feature
        { value: "test",            name: "test:            a new test" },//create: add test
        { value: "bugfix",          name: "bugfix:          a bug(before release) fix" },//update
        { value: "hotfix",          name: "hotfix:          a bug(after release) fix" },//update
        { value: "improve",         name: "improve:         a change that maybe add some small functions or improves quality"},//update
        { value: "perf",            name: "perf:            a change that improves performance" },//update performance
        { value: "refactor",        name: "refactor:        a change that neither fixes a bug nor adds a feature" },//update: non-feature
        { value: "remove",          name: "remove:          remove something"},//delete

        //the devops
        // { value: "chore",   name: "chore:       changes to the build process or auxiliary tools and libraries such as documentation generation" },//chore
        // { value: "repo-pipelines",  name: "repo-pipelines:  a change about the build process or auxiliary tools and libraries such as documentation generation" },//pipeline
        // { value: "repo-config",     name: "repo-config:     a change about the repo-config, but not affect the pipelines"},//repo-config
        // { value: "repo",            name: "repo:            a change about the repo, include repo-pipelines and repo-config"},//repo-config

        //the backward crud
        { value: "revert",          name: "revert:          revert to a commit" },//update: revert

        //mix
        { value: "mix",             name: "mix:             multi-purposes"},//create, update, delete: more than one action

        //warning: wip will not set the scope
        { value: "wip",             name: "wip:             work in progress" },//unable to classify

        // { value: "docs",    name: "docs:        Documentation only changes" },
        // { value: "style",   name: "style:       Changes that do not affect the meaning of the code(white-space, formatting, missing semi-colons, etc)" },
    ],

    scopes: [
        //logic
        { name: "readme" },
        { name: "docs-readme" },
        { name: "abouts" },
        { name: "addons" },
        { name: "exercises" },
        { name: "experiments" },
        { name: "glossaries" },
        { name: "lectures" },
        { name: "slides" },
        { name: "codes-readme" },
        { name: "codes" },
        { name: "misc" },

        //infra
        { name: "infra" },
    ],

    allowTicketNumber: true,
    isTicketNumberRequired: false,
    ticketNumberPrefix: "CSF-",
    ticketNumberRegExp: "\\d{1,5}",

    // it needs to match the value for field type. Eg.: 'fix'
    /*
    scopeOverrides: {
        fix: [

        {name: 'merge'},
        {name: 'style'},
        {name: 'e2eTest'},
        {name: 'unitTest'}
        ]
    },
    */
    // override the messages, defaults are as follows
    messages: {
        type: "select the type of change that you're committing:",
        scope: "\ndenote the SCOPE of this change (optional):",
        // used if allowCustomScopes is true
        customScope: "denote the SCOPE of this change:",
        subject: "write a SHORT, IMPERATIVE tense description of the change:\n",
        body: 'provide a LONGER description of the change (optional). use "|" to break new line:\n',
        breaking: "list any BREAKING CHANGES (optional):\n",
        footer: "List any ISSUES CLOSED by this change (optional). E.g.: #31, #34:\n",
        confirmCommit: "are you sure you want to proceed with the commit above?",
    },

    allowCustomScopes: true,
    allowBreakingChanges: ["feat", "bugfix", "hotfix"],
    // skip any questions you want
    skipQuestions: ["body", "footer"],

    // limit subject length
    subjectLimit: 100,
    // breaklineChar: '|', // It is supported for fields body and footer.
    // footerPrefix : 'ISSUES CLOSED:'
    // askForBreakingChangeFirst : true, // default is false
};
