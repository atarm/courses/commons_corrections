# 书籍非官方修正

## 许可协议

![CC-BY-SA-4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)

采用`CC-BY-SA-4.0`协议，署名：aRoming。

## 关于本非官方修正的说明

非官方修正： 在未说明情况下，相关修正均为非官方修正，也未取得出版社或原著（或译者）确认，因此，仅供参考
